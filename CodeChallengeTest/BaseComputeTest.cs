﻿using System;
using CodeChallenge;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CodeChallengeTest
{
    [TestClass]
    public class BaseComputeTest
    {
        [TestMethod]
        public void BaseComputeReturnsAString()
        {
            //Arrange
            decimal _sizeMultiplier = 1m;
            int _fieldMultiplier = 1;
            BaseCompute compute = new BaseCompute(_sizeMultiplier, _fieldMultiplier);

            //Act
            Type returnType = compute.Compute().GetType();

            //Assert
            Assert.IsTrue(returnType == typeof(string));
        }
    }
}
