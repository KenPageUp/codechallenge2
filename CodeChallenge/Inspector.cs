﻿using System;

namespace CodeChallenge
{
    public class Inspector
    {
        private Parcel _parcel;

        public Inspector(Parcel parcel)
        {
            _parcel = parcel;
        }

        public IClassification Process()
        {
            EnumCategory category = this.Inspect();
            IClassification classification = this.Evaluate(category);
            return classification;
        }

        private IClassification Evaluate(EnumCategory category)
        {
            switch (category)
            {
                case EnumCategory.Reject:
                    _parcel.classification = new RejectParcel();
                    break;
                case EnumCategory.HeavyParcel:
                    _parcel.classification = new HeavyParcel(_parcel.Weight);
                    break;
                case EnumCategory.SmallParcel:
                    _parcel.classification = new SmallParcel(_parcel.Volume);
                    break;
                case EnumCategory.MediumParcel:
                    _parcel.classification = new MediumParcel(_parcel.Volume);
                    break;
                case EnumCategory.LargeParcel:
                    _parcel.classification = new LargeParcel(_parcel.Volume);
                    break;
                default:
                    // A new classification may have been added.
                    throw new Exception();
            }

            return _parcel.classification;
        }

        private EnumCategory Inspect()
        {
            EnumCategory category;

            if (_parcel.Weight > 50)
            {
                category = EnumCategory.Reject;
            }
            else if (_parcel.Weight > 10)
            {
                category = EnumCategory.HeavyParcel;
            }
            else if (_parcel.Volume < 1500)
            {
                category = EnumCategory.SmallParcel;
            }
            else if (_parcel.Volume < 2500)
            {
                category = EnumCategory.MediumParcel;
            }
            else
            {
                category = EnumCategory.LargeParcel;
            }

            return category;
        }
    }
}