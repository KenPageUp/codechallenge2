﻿namespace CodeChallenge
{
    enum EnumCategory
    {
        Reject,
        HeavyParcel,
        SmallParcel,
        MediumParcel,
        LargeParcel
    }
}