﻿using System;

namespace CodeChallenge
{
    class Program
    {
        static void Main(string[] args)
        {
            bool restart = true;

            while (restart)
            {
                TakeTheSurvey(out int weight, out int height, out int width, out int depth);

                try
                {
                    Parcel parcel = new Parcel(weight, height, width, depth);
                    var inspector = new Inspector(parcel);
                    IClassification result = inspector.Process();

                    Console.WriteLine();
                    Console.WriteLine(string.Format("Category: {0}", result.ToString()));
                    Console.WriteLine();
                    Console.WriteLine(string.Format("Cost: {0}", result.Compute()));
                    Console.WriteLine();
                    Console.WriteLine("Type Y to restart");
                    restart = Console.ReadLine().ToUpper() == "Y" ? true : false;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("An error has occured, we'll close the application");
                    Console.ReadKey();
                    Environment.Exit(0);
                }

                if (restart)
                {
                    Console.Clear();
                }
            }
        }

        private static void TakeTheSurvey(out int weight, out int height, out int width, out int depth)
        {
            Console.Write("Enter Weight in kg: ");
            while (int.TryParse(Console.ReadLine(), out weight).Equals(false))
            {
                Console.WriteLine("Invalid input! Please supply a valid numeric value.");
                Console.Write("Enter Weight in kg: ");
            }

            Console.Write("Enter Height in cm: ");
            while (int.TryParse(Console.ReadLine(), out height).Equals(false))
            {
                Console.WriteLine("Invalid input! Please supply a valid numeric value.");
                Console.Write("Enter Height in cm: ");
            }

            Console.Write("Enter Width in cm: ");
            while (int.TryParse(Console.ReadLine(), out width).Equals(false))
            {
                Console.WriteLine("Invalid input! Please supply a valid numeric value.");
                Console.Write("Enter Width in cm: ");
            }

            Console.Write("Enter Depth in cm: ");
            while (int.TryParse(Console.ReadLine(), out depth).Equals(false))
            {
                Console.WriteLine("Invalid input! Please supply a valid numeric value.");
                Console.Write("Enter Depth in cm: ");
            }
        }
    }
}