﻿namespace CodeChallenge
{
    public class Parcel
    {
        public Parcel(int weight, int height, int width, int depth)
        {
            this.Weight = weight;
            this.Height = height;
            this.Width = width;
            this.Depth = depth;
        }

        public int Weight { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public int Depth { get; set; }
        public int Volume => Height * Width * Depth;

        public IClassification classification;
    }
}
