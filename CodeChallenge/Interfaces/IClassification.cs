﻿namespace CodeChallenge
{
    public interface IClassification
    {
        string Compute();
    }
}