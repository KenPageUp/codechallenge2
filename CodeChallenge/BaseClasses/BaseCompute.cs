﻿using System;

namespace CodeChallenge
{
    public class BaseCompute
    {
        decimal _sizeMultiplier;
        int _fieldMultiplier;

        public BaseCompute(decimal sizeMultiplier, int fieldMultiplier)
        {
            _sizeMultiplier = sizeMultiplier;
            _fieldMultiplier = fieldMultiplier;
        }

        public virtual string Compute()
        {
            var resultString = string.Format("${0}", Convert.ToString(_sizeMultiplier * _fieldMultiplier));

            return resultString;
        }
    }
}
