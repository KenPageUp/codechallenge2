﻿using System;

namespace CodeChallenge
{
    class HeavyParcel : BaseCompute, IClassification
    {
        const decimal MULTIPLIER = 15m;

        public HeavyParcel(int weight) : base(MULTIPLIER, weight)
        {
        }

        public override string ToString()
        {
            return "Heavy Parcel";
        }
    }
}
