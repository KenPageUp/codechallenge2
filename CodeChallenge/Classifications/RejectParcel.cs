﻿namespace CodeChallenge
{
    class RejectParcel : IClassification
    {
        public string Compute()
        {
            return "N/A";
        }

        public override string ToString()
        {
            return "Rejected";
        }
    }
}