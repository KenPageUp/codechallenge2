﻿namespace CodeChallenge
{
    class SmallParcel : BaseCompute, IClassification
    {
        const decimal MULTIPLIER = .05m;

        public SmallParcel(int volume) : base(MULTIPLIER, volume)
        {
        }

        public override string ToString()
        {
            return "Small Parcel";
        }
    }
}