﻿using System;

namespace CodeChallenge
{
    class MediumParcel : BaseCompute, IClassification
    {
        const decimal MULTIPLIER = .04m;

        public MediumParcel(int volume) : base(MULTIPLIER, volume)
        {
        }

        public override string ToString()
        {
            return "Medium Parcel";
        }
    }
}
