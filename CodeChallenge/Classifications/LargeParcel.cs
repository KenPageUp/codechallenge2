﻿namespace CodeChallenge
{
    class LargeParcel : BaseCompute, IClassification
    {
        const decimal MULTIPLIER = .03m;

        public LargeParcel(int volume) : base(MULTIPLIER, volume)
        {
        }

        public override string ToString()
        {
            return "Large Parcel";
        }
    }
}